# GithubIssuesUI
https://bitbucket.org/alatz3d/githubissuesui/src/master/

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.

## Instalaci�n
npm install -g @angular/cli (Requerido: En caso de no tenerlo instalado)
### Creaci�n
ng new GithubIssuesUI
npm install bootstrap@3 jquery ngx-bootstrap underscore --save
### Si copia el proyecto
Clonar con git y ejecutar "npm install" para descragar los modulos de node 

## Probar test
ng e2e


## Development server (Angular)

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding (Angular)

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build (Angular)

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests (Angular)

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests (Angular)

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help (Angular)

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
