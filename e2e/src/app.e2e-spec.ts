import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('debe de mostrar el título', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('GithubIssuesUI');
  });
});
