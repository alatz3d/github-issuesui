import { Component } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { PagerService } from './_services/pager.service';
@Component({
  selector: 'app-issues',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'GithubIssuesUI';

  validado:boolean;
  url:string;
  partesUrl:string[];
  cargado:boolean;

  constructor(private http:HttpClient, private pagerService: PagerService){  }
  /* Paginación:  */
    // array of all items to be paged
    private allItems: any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];

    setPage(page: number) {
      if (page < 1 || page > this.pager.totalPages) {
          return;
      }

      // get pager object from service
      this.pager = this.pagerService.getPager(this.allItems.length, page);

      // get current page of items
      this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

  onGithubUrlKeyUp(event:any){
    let urlTmp = event.target.value.trim();
    this.cargado = false;

    /* Validar URL */
      /* Tests:
        https://github.com/octocat/Hello-World
        https://github.com/octocat/Hello-World/
        https://github.com/octocat/Hello-World/issues
        https://github.com/octocat/Hello-World/issues/
      */
    
    //Comprobar es de github
    let res = urlTmp.match(/^((?:http:\/\/github.com\/)|(?:https:\/\/github.com\/))(\/)?([-\/a-zA-Z0-9\.]*)$/gm);
    if(res == null) {
      this.validado = false;
    } else {
      //La URL debe contener "/" min 4. max 6
      let contaBarras = urlTmp.split('/').length-1;
      if (contaBarras >= 4 && contaBarras <= 6) {
        this.validado = true;
        this.url = urlTmp; 
      } else {
        this.validado = false;
      }
    }
  }
  GetGithubIssues(){   
    /* Tests:
      https://github.com/octocat/Hello-World
      ===> concvertir ===>
      https://api.github.com/repos/octocat/Hello-World/issues
    */
   let corteUrl = this.url.indexOf("github.com");
   this.partesUrl= this.url.substr(corteUrl+10).split("/");

    //Realizar petición con Observable
    console.log('https://api.github.com/repos/'+this.partesUrl[1]+'/'+this.partesUrl[2]+'/issues');
    this.http.get('https://api.github.com/repos/'+this.partesUrl[1]+'/'+this.partesUrl[2]+'/issues')
    .subscribe(
      (data:any[]) => {
        if (data.length) {
          this.cargado = true;
          /* Elementos para paginación */
          // set items to json response
          this.allItems = data;
          // initialize to page 1
          this.setPage(1);
        }
      }
    )
  }

  //Convertir url de api.github a web url de github
  urlAPItoWeb(url:string){
    let array = url.split("/");
    let urlWeb = "https://github.com/"+this.partesUrl[1]+'/'+this.partesUrl[2]+"/issues/"+array[array.length-1];
    return urlWeb;
  }

}
